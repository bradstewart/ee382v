package com.beastmode.servlets;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.*;

import com.beastmode.classes.BeastImage;
import com.beastmode.classes.Stream;

@SuppressWarnings("serial")
public class ViewStreamServlet extends HttpServlet {
	private static final Logger log = Logger.getLogger(ViewStreamServlet.class.getName());
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		Long streamId = new Long(req.getParameter("streamId"));
		log.info("VIEWING STREAM");
		//String streamName = request.getParameter("streamName");
		//out.println(streamName);
		Stream stream = Stream.fetchStream( streamId );
		log.info(Long.toString(stream.id));
		List<BeastImage> images = stream.getImages();
//				OfyService.ofy().load().type(BeastImage.class).list();
		Collections.sort(images);
		StringBuilder sb = new StringBuilder();
		for ( BeastImage img : images ) {
			sb.append("<div class=\"col-md-3\"><div class=\"thumbnail\">");
			sb.append("<img src=\""+img.bkUrl+"\" ");
			sb.append("<a href=\""+img.bkUrl+"\"></a>");
			sb.append("<div class=\"caption\">");
			sb.append("<p>"+img.comments+"</p>");
//			String url = "/view?streamId="+stream.id;
//			sb.append("<p><a href=\""+img.bkUrl+"\" class=\"btn btn-primary\"></a></p>");
			sb.append("</div></div></div>");
//     		  out.println("<img src=\"" + img.bkUrl + "\">"); // better to not use println for html output, use templating instead
     		}
		System.out.println(sb.toString());
		resp.getWriter().write( sb.toString() );
    }
	
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doGet( req, resp );
	}
}