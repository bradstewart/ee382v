package com.beastmode.servlets;

import java.io.IOException;
import java.util.Date;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.beastmode.classes.BeastUser;
import com.beastmode.classes.Stream;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@SuppressWarnings("serial")
public class CreateStreamServlet extends HttpServlet {
	private static final Logger log = Logger.getLogger(CreateStreamServlet.class.getName());
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();
		
		Stream s = new Stream(req.getParameter("name"),
				req.getParameter("tags"),
				req.getParameter("creator"),
				req.getParameter("subscribers"),
				req.getParameter("message"),
				req.getParameter("cover"));
		
		// persist to datastore
		BeastUser beast = BeastUser.fetchUser( req.getParameter("creator") );
		s.save();
	
		beast.ownStream( s.id );
		beast.save();
		
		resp.sendRedirect("/viewall.jsp");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doGet( req, resp );
	}
}