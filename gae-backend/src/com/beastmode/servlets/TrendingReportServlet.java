package com.beastmode.servlets;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.*;

import com.beastmode.classes.Stream;
import com.beastmode.classes.StreamViewComparator;
import com.beastmode.classes.TrendingRate;
import com.beastmode.classes.TrendingStreams;

import static com.beastmode.util.OfyService.ofy;

@SuppressWarnings("serial")
public class TrendingReportServlet extends HttpServlet {
	private static final Logger log = Logger.getLogger(TrendingReportServlet.class.getName());
	 //mail id from which the mail has to be sent
	  private static String fromAddress = "bstew92@gmail.com";
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
				
		String value = req.getParameter("schedule");
		if (value.equals("fivemin") ) {
			updateTrendingStreams();
		}
		
		TrendingRate tr = TrendingRate.fetchRate();
		
		if ( value.equals(tr.value) ) {
			//Send emails and shit.
			String subject = "[Beast Mode] Trending Stream Report";
			String body = "Here's some stuff about some stuff";
			String toAddress = "bstew92@gmail.com";
			List<Stream> streams = TrendingStreams.fetchStreams();
			StringBuilder sb = new StringBuilder();
			sb.append("Currently Trending Streams \n Reported every "+value+"\n \n");
			for ( Stream stream : streams ) {
				sb.append(stream.name+" created by "+stream.owner+" with "+stream.getViewCount()+" views.\n");
			}
			
			send(toAddress, subject, sb.toString());
			log.severe("yay emails");
		}
				
	}
	 

	private void updateTrendingStreams() {
		List<Stream> streams = Stream.fetchStreams();
		Collections.sort(streams, new StreamViewComparator());
		TrendingStreams ts = new TrendingStreams();
		for ( int i=0; i < 3 && i < streams.size(); i++ ) {
			ts.add( streams.get(i) );
		}
		ts.save();
	}
	 

	  /**
	   * Method defines the way to send a mail
	   *
	   * @param toAddress : the address to which mail needs to be sent
	   * @param subject : subject of the mail
	   * @param msgBody : mail content
	   * @throws IOException
	   */
	  // Send the Mail
	  public void send(String toAddress, String subject, String msgBody)
	      throws IOException {

	    Properties props = new Properties();
	    Session session = Session.getDefaultInstance(props, null);

	    try {
	      Message msg = new MimeMessage(session);
	      msg.setFrom(new InternetAddress(fromAddress));
	      InternetAddress to = new InternetAddress(toAddress);
	      msg.addRecipient(Message.RecipientType.TO, to);
	      InternetAddress to2 = new InternetAddress("kamran.ks+aptmini@gmail.com");
	      msg.addRecipient(Message.RecipientType.TO, to2);
	      msg.setSubject(subject);
	      msg.setText(msgBody);
	      Transport.send(msg, new InternetAddress[] { to });

	    } catch (AddressException addressException) {
	      log.severe("Address Exception , mail could not be sent" + addressException);
	    } catch (MessagingException messageException) {
	      log.severe("Messaging Exception , mail could not be sent" + messageException);
	    }
	  }
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doGet( req, resp );
	}
}