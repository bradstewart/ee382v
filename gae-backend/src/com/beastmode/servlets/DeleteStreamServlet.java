package com.beastmode.servlets;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.beastmode.classes.BeastUser;
import com.beastmode.classes.Stream;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@SuppressWarnings("serial")
public class DeleteStreamServlet extends HttpServlet {
	private static final Logger log = Logger.getLogger(DeleteStreamServlet.class.getName());
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		UserService userService = UserServiceFactory.getUserService();
//		User user = userService.getCurrentUser();
		
		Gson gson = new Gson();
		log.info( req.getRequestURI() );
		log.info( req.getParameter("params") );
		log.info( req.getParameter("user") );
		List<Long> streamIds = Arrays.asList(gson.fromJson( req.getParameter("params"), Long[].class));
		BeastUser beast = BeastUser.fetchUser( req.getParameter("user") );
		for ( Long id : streamIds ) {
			log.info( id.toString() );
			Stream stream = Stream.fetchStream(id);
			stream.delete();
			beast.disownStream( id );
		}
		
		beast.save();
		
		
		// persist to datastore
		//resp.sendRedirect("/manage.jsp");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doGet( req, resp );
	}
}
