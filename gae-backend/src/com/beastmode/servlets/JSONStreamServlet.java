package com.beastmode.servlets;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.*;

import com.beastmode.classes.BeastImage;
import com.beastmode.classes.Stream;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@SuppressWarnings("serial")
public class JSONStreamServlet extends HttpServlet {
	private static final Logger log = Logger.getLogger(JSONStreamServlet.class.getName());
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		Long streamId = new Long(req.getParameter("streamId"));
		log.info("JSON Stream Servlet");
		
		Stream stream = Stream.fetchStream( streamId );
		
		log.info("Reading Stream "+Long.toString(stream.id));
		
		List<BeastImage> images = stream.getImages();
		Collections.sort(images);
		
		JsonElement jsonTree = new Gson().toJsonTree(images);
		JsonObject json = new JsonObject();
		json.add("images", jsonTree);
		
	
		log.severe( json.toString() );
		resp.getWriter().write( json.toString() );
    }
	
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doGet( req, resp );
	}
}