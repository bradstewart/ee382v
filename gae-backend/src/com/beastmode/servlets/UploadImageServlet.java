package com.beastmode.servlets;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mortbay.log.Log;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.beastmode.classes.BeastImage;

import static com.beastmode.util.OfyService.ofy;

@SuppressWarnings("serial")
public class UploadImageServlet extends HttpServlet {
	private static final Logger log = Logger.getLogger(UploadImageServlet.class.getName());
	private BlobstoreService blobstoreService = BlobstoreServiceFactory
			.getBlobstoreService();

	@SuppressWarnings("deprecation")
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();
		// APT: dont worry that the getUploadedBlobs/getServingUrl are
		// deprecated
		Map<String, BlobKey> blobs = blobstoreService.getUploadedBlobs(req);
		// the "myFile" keyword comes from the upload form in ShowSream.jsp
		for ( String str : blobs.keySet() ) {
			log.severe("Blob Name :"+ str +" Key: "+ blobs.get(str).getKeyString() );
		}
		BlobKey blobKey = blobs.get("files");

		// APT: the BlobKey is not a url, so we use ImagesServiceFactory to get
		// a URL to serve the image stored in the blob from
		ImagesService imagesService = ImagesServiceFactory.getImagesService();
		String bkUrl = imagesService.getServingUrl(blobKey);

		// note that these parameters are set when we do the
		// blobstoreService.createUploadUrl() call
		// in ShowStream.jsp
		Long streamId = new Long(req.getParameter("streamId"));
		// at a later day we might allow for tags on a per image basis
		String content = req.getParameter("comments");

		BeastImage s = new BeastImage(streamId, req.getParameter("user"), content, bkUrl);
		s.save();
		// after the creation of the image is complete we want to return to the
		// stream that it was added to
		// we use the streamId as an argument to the ShowStream jsp
		// NOTE: in the "/ShowStream..." below if we left off the leading / we 
		// would get a relative redirect, which is not what we want
		res.sendRedirect("/viewstream.jsp?streamId=" + streamId);
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
//		doPost( req, resp );
	}
}