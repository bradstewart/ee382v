package com.beastmode.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedSet;
import java.util.logging.Logger;

import javax.servlet.http.*;

import com.beastmode.classes.BeastImage;
import com.beastmode.classes.Stream;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import edu.macalester.acs.AutocompleteEntry;
import edu.macalester.acs.AutocompleteTree;

@SuppressWarnings("serial")
public class JSONAutocompleteServlet extends HttpServlet {
	private static final Logger log = Logger.getLogger(JSONAutocompleteServlet.class.getName());
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String term = req.getParameter("term");
//		AutocompleteTree<Integer, String> tree = new AutocompleteTree<Integer, String>();
//		ArrayList<String> words = new ArrayList<String>();
//		tree.add(1, "test"); tree.add(2,"team"); tree.add(3,"tear");
		
		//Stream stream = Stream.fetchStream( streamId );
		
		//log.info("Reading Stream "+Long.toString(stream.id));
		
		//List<BeastImage> images = stream.getImages();
		//Collections.sort(images);
		
//		SortedSet<AutocompleteEntry<Integer, String>> results = tree.autocomplete(term, 20);
		List<Stream> allStreams = Stream.fetchStreams();

		ArrayList<String> fields = new ArrayList<String>();
		for( Stream stream : allStreams) {
			String allFields = stream.name + " " + stream.owner + " " + stream.tags;
			allFields = allFields.replace(",", "").replace("#", "").toLowerCase();
			Collections.addAll(fields, allFields.split(" "));
		}
		
		ArrayList<String> result = new ArrayList<String>();
		
		for ( String field : fields ) {
			if ( field.startsWith(term) ) {
				result.add( field );
			}
		}
		
		Collections.sort( result );

		String json = new Gson().toJson(result);
//		JsonObject json = new JsonObject();
//		json.add("images", jsonTree);
		
	
		log.severe( json );
		resp.getWriter().write( json );
    }
	
    
	
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doGet( req, resp );
	}
}