
package com.beastmode.servlets;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.*;

import com.beastmode.classes.Stream;
import com.beastmode.classes.StreamViewComparator;
import com.beastmode.classes.TrendingStreams;

@SuppressWarnings("serial")
public class TrendingStreamsServlet extends HttpServlet {
	private static final Logger log = Logger.getLogger(TrendingStreamsServlet.class.getName());
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
//		List<Stream> streams = Stream.fetchStreams();
//		Collections.sort(streams, new StreamViewComparator());
		List<Stream> streams = TrendingStreams.fetchStreams();
		StringBuilder sb = new StringBuilder();
		for ( int i=0; i < 3 && i < streams.size(); i++ ) {
				sb.append("<div class=\"als-item\"><div class=\"thumbnail\">");
				sb.append("<img style=\"height: 300px;width: 300px;\" src=\""+streams.get(i).coverImageUrl+"\"> ");
				sb.append("<div class=\"caption\">");
				sb.append("<h3>"+streams.get(i).name+"</h3>");
				sb.append("<p>"+streams.get(i).tags+"</p>");
				sb.append("<p>"+streams.get(i).getViewCount()+" views in last hour."+"</p>");
				String url = "/viewstream.jsp?streamId="+streams.get(i).id;
				sb.append("<p><a href=\""+url+"\" class=\"btn btn-primary\">View</a></p>");
				sb.append("</div></div></div>");
			}
		resp.setContentType("text/html");
		resp.getWriter().println(sb.toString());
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doGet( req, resp );
	}
}