package com.beastmode.servlets;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.*;

import com.beastmode.classes.Stream;

@SuppressWarnings("serial")
public class ViewAllStreamsServlet extends HttpServlet {
	private static final Logger log = Logger.getLogger(ViewAllStreamsServlet.class.getName());
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		List<Stream> streams = Stream.fetchStreams();
		log.info("ViewAllServlet hit.");
		System.out.println(streams.size());
		Collections.sort(streams);
		StringBuilder sb = new StringBuilder();
		for ( Stream stream : streams ) {			
			sb.append("<div class=\"als-item\"><div class=\"thumbnail\">");
			sb.append("<img style=\"height: 200px;width: 200px;\" src=\""+stream.coverImageUrl+"\"> ");
			sb.append("<a href=\""+stream.coverImageUrl+"\"></a>");
			sb.append("<div class=\"caption\">");
			sb.append("<h3>"+stream.name+"</h3>");
			sb.append("<p>"+stream.tags+"</p>");
			String url = "/viewstream.jsp?streamId="+stream.id;
			String mapUrl = "/viewmap.jsp?streamId="+stream.id;
			sb.append("<p><a href=\""+url+"\" class=\"btn btn-primary\">View</a>");
			sb.append("<a href=\""+mapUrl+"\" class=\"btn btn-primary\">Map</a></p>");
			sb.append("</div></div></div>");
		}
		resp.setContentType("text/html");
		resp.getWriter().println(sb.toString());
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doGet( req, resp );
	}
}