package com.beastmode.servlets;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.*;

import com.beastmode.classes.Stream;
import com.beastmode.classes.StreamViewComparator;
import com.beastmode.classes.TrendingRate;

import static com.beastmode.util.OfyService.ofy;

@SuppressWarnings("serial")
public class TrendingRateServlet extends HttpServlet {
	private static final Logger log = Logger.getLogger(TrendingRateServlet.class.getName());
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
				
		String value = req.getParameter("updateRate");
		log.info( value );
		if ( value.equals("none") ) {
			new TrendingRate("none").save();
		} else if ( value.equals("five-min") ) {
			new TrendingRate("fivemin").save();
		} else if ( value.equals("hour") ) {
			new TrendingRate("hour").save();
		} else if ( value.equals("day") ) {
			new TrendingRate("day").save();
		}
		
		resp.sendRedirect("/trending.jsp");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doGet( req, resp );
	}
}