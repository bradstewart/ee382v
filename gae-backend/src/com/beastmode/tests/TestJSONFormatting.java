package com.beastmode.tests;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.beastmode.api.UploadedImage;
import com.beastmode.classes.BeastImage;
import com.beastmode.classes.ImageDistanceComparator;
import com.beastmode.classes.Stream;
import com.beastmode.util.Distance;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.base.Strings;
import com.google.common.io.CharStreams;
import com.google.common.io.Files;
import com.google.common.io.InputSupplier;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class TestJSONFormatting {
	
	 private final LocalServiceTestHelper helper =
		        new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());


	@Before
	public void setUp() throws Exception {
		 helper.setUp();
		 makeStreams();		 
	}

	@After
	public void tearDown() throws Exception {
		helper.tearDown();
	}
	@Test
	public void hitLive() throws IOException {
		URL url = new URL("http://ericissunny.appspot.com/streamservlet?type=single&id=5209829689786368");
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setRequestMethod("GET");
		connection.connect();
		String string = CharStreams.toString( new InputStreamReader( connection.getInputStream(), "UTF-8" ) );
		Gson gson = new Gson();

		System.out.println(gson.fromJson(string, new TypeToken<Map<Stream, List<BeastImage>>>(){}.getType() ) );

	}
	@Test
	public void testNearby() {
		List<BeastImage> images = BeastImage.fetchImages();
		
		// Sort streams by distance in Km
		GeoPoint location = new GeoPoint(Math.random() * Math.PI * 2, Math.acos(Math.random() * 2 - 1));
		Comparator<BeastImage> cp = new ImageDistanceComparator( location );
		Collections.sort(images, cp);
		for ( BeastImage image : images ) {
			System.out.println(image.location.getLatitude() + ":" + image.location.getLongitude());
		}
	
	}
	@Test
	public void fromSingleJson() throws MalformedURLException {
		Map<Stream, List<BeastImage>> result = new HashMap<Stream, List<BeastImage>>();
		
		List<Stream> streams = Stream.fetchStreams();
		List<Long> ids = new ArrayList<Long>();
		for ( Stream stream : streams ) {
			ids.add( stream.id );
		}
		
		Random r = new Random();
		
		int i = r.nextInt( ids.size() );

		Stream stream = Stream.fetchStream( ids.get(i) );		
			result.put(stream, stream.getImages());

		GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
		Type t = new TypeToken<HashMap<Stream,List<BeastImage>>>(){}.getType();
		String json = gson.toJson(result, t);
		
		gson = new Gson();
		
		HashMap<Stream, List<BeastImage>> rec = gson.fromJson(json, t);
		
		for ( Map.Entry<Stream, List<BeastImage>> entry : rec.entrySet() ) {
			if ( Strings.isNullOrEmpty( entry.getKey().name ) ) { fail("Stream name null or empty."); }
			if ( entry.getValue().size() != 5 ) { fail("Expected 5 images, had "+ entry.getValue().size() ); }
		}
		
	}
	
	@Test
	public void testAllStreams() throws MalformedURLException {
		Map<Stream, List<BeastImage>> result = new HashMap<Stream, List<BeastImage>>();
//		Map<Stream, String> result = new HashMap<Stream, String>();
		
		
			List<Stream> streams = Stream.fetchStreams();
			Collections.sort(streams);
			for ( Stream stream : streams ) {
				result.put(stream, stream.getImages());
			}
			
		
		GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
		Type t = new TypeToken<HashMap<Stream,List<BeastImage>>>(){}.getType();
		String json = gson.toJson(result, t);
		
		gson = new Gson();
		
		HashMap<Stream, List<BeastImage>> rec = gson.fromJson(json, t);
		
		for ( Map.Entry<Stream, List<BeastImage>> entry : rec.entrySet() ) {
			if ( Strings.isNullOrEmpty( entry.getKey().name ) ) { fail("Stream name null or empty."); }
			if ( entry.getValue().size() != 5 ) { fail("Expected 5 images, had "+ entry.getValue().size() ); }
		}
		
	}
	
	@Test
	public void fromNearbyStreams() throws MalformedURLException {
		
		String json = null;
		Gson gson = new Gson();

		GeoPoint location = new GeoPoint(Math.random() * Math.PI * 2, Math.acos(Math.random() * 2 - 1));
		List<BeastImage> images = BeastImage.fetchImages();
		System.out.println(images.size()+" nearby images.");
		
		// Sort streams by distance in Km
		Comparator<BeastImage> cp = new ImageDistanceComparator( location );

		Collections.sort(images, cp);
		
		for ( int i=0; i<images.size()-2; i++ ) {
			if ( Distance.calculate(location, images.get(i).location, 'K') >  
			Distance.calculate(location, images.get(i+1).location, 'K') ) {
				fail("Not sorted. "+i);
			}
		}

		Type t = new TypeToken<List<BeastImage>>(){}.getType();
		json = gson.toJson(images, t);
		
		gson = new Gson();
		Type m = new TypeToken<List<BeastImage>>(){}.getType();
		List<BeastImage> res = gson.fromJson(json, m);
		
		for ( BeastImage img : res ) {
			if ( Strings.isNullOrEmpty( img.bkUrl) ) { fail("Image url is null or empty."); }
			if ( res.size() != 100 ) { fail("Expected 100 images, had "+ res.size() ); }
		}

	}
	
	@Test
	public void uploadServlet() throws IOException {
		String tstJson;
	  	byte[] b ={} ;

		String[] fileNames = {
		"war/Sample_Streams/flags_colored/Cover.png",
		"war/Sample_Streams/flags_colored/Flag_China.png",
		"war/Sample_Streams/flags_colored/Flag_Japan.png",
		};
		List<Stream> streams = Stream.fetchStreams();
		List<Long> ids = new ArrayList<Long>();
		for ( Stream stream : streams ) {
			ids.add( stream.id );
		}
		
		Random r = new Random();
		
		int i = r.nextInt( ids.size() );;

		for ( String fn : fileNames ) {
	    		try {
	      			b = Files.toByteArray( new File(fn));
	    		} catch (Exception e) {
	      			e.printStackTrace();
	    		}
	    		UploadedImage img = new UploadedImage( ids.get(i), "OWNED", "TEXTABOUTME", b);
	    		Gson gson = new Gson();
	    		tstJson = gson.toJson(img);
	       		
	       		
	       		String json = CharStreams.toString( new InputStreamReader( new ByteArrayInputStream(tstJson.getBytes("UTF-8") )));
	    		BufferedReader br = new BufferedReader(new InputStreamReader( new ByteArrayInputStream(b)) );
	    		StringBuilder sb = new StringBuilder();
	       		String str;
//	    		while ((str = br.readLine()) != null) {
//	    			sb.append(str);
//	    		}
//	    		// JSONObject jObj = new JSONObject(sb.toString());
//	    		String tstJson = sb.toString();
	    		img = UploadedImage.fromJson(json);
	    		img.uploadToBlobstore();
	    		BeastImage beastImg = img.toBeastImage();
	    		beastImg.save();
	    		if ( Strings.isNullOrEmpty( beastImg.bkUrl ) ) { fail("Img URL null or empty."); }
		}
	}
	
//	@Test
	public void fromSearchStreams() throws MalformedURLException {
		Map<Stream, List<BeastImage>> result = new HashMap<Stream, List<BeastImage>>();
//		Map<Stream, String> result = new HashMap<Stream, String>();
		

//			result = searchStreams( req.getParameter("query") );
		

		GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
		Type t = new TypeToken<HashMap<Stream,List<BeastImage>>>(){}.getType();
		String json = gson.toJson(result, t);
		
		gson = new Gson();
		
		HashMap<Stream, List<BeastImage>> rec = gson.fromJson(json, HashMap.class);
		
		for ( Map.Entry<Stream, List<BeastImage>> entry : rec.entrySet() ) {
			if ( Strings.isNullOrEmpty( entry.getKey().name ) ) { fail("Stream name null or empty."); }
			if ( entry.getValue().size() != 5 ) { fail("Expected 5 images, had "+ entry.getValue().size() ); }
		}
		
	}

	@Test
	public void test() {
		Gson gson = new Gson();
		Map<Stream, List<BeastImage>> result = new HashMap<Stream, List<BeastImage>>();
		

		List<Stream> streams = Stream.fetchStreams();
		Collections.sort(streams);
		for ( Stream stream : streams ) {
			result.put(stream, stream.getImages());
		}
		String json = gson.toJson(result);
	}
	
	private void makeStreams() {
		for ( int i=0; i<20; i++ ) {
			Stream s = new Stream("name"+i, "tags"+i, "owner"+i,"subscribers"+i,"welcome"+i,"url"+i);
			s.save();
			for ( int j=0; j<5; j++ ) {
				BeastImage img = new BeastImage(s.id, "owner"+i+j,"content"+i+j, "url"+i+j,
						new GeoPoint(Math.random() * Math.PI * 2, Math.acos(Math.random() * 2 - 1)));
				img.save();
			}
		}
	}

}
