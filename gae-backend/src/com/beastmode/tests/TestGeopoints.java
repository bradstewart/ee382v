package com.beastmode.tests;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.beastmode.classes.BeastImage;
import com.beastmode.classes.ImageDistanceComparator;
import com.beastmode.classes.Stream;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.base.Strings;
import com.google.common.io.CharStreams;
import com.google.common.io.InputSupplier;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class TestGeopoints {
	
	 private final LocalServiceTestHelper helper =
		        new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());


	@Before
	public void setUp() throws Exception {
		 helper.setUp();
		 makeStreams();		 
	}

	@After
	public void tearDown() throws Exception {
		helper.tearDown();
	}
//	@Test
	public void hitLive() throws IOException {
		URL url = new URL("http://ericissunny.appspot.com/streamservlet?type=single&id=5209829689786368");
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setRequestMethod("GET");
		connection.connect();
		String string = CharStreams.toString( new InputStreamReader( connection.getInputStream(), "UTF-8" ) );
		Gson gson = new Gson();

		System.out.println(gson.fromJson(string, new TypeToken<Map<Stream, List<BeastImage>>>(){}.getType() ) );

	}
	
	@Test
	public void testNearby() {
		List<BeastImage> images = BeastImage.fetchImages();
		
		// Sort streams by distance in Km
		GeoPoint location = new GeoPoint(Math.random() * Math.PI * 2, Math.acos(Math.random() * 2 - 1));
		Comparator<BeastImage> cp = new ImageDistanceComparator( location );
		Collections.sort(images, cp);
	}
	
	@Test
	public void fromSingleJson() throws MalformedURLException {
		Map<Stream, List<BeastImage>> result = new HashMap<Stream, List<BeastImage>>();
		
		List<Stream> streams = Stream.fetchStreams();
		List<Long> ids = new ArrayList<Long>();
		for ( Stream stream : streams ) {
			ids.add( stream.id );
		}
		
		Random r = new Random();
		
		int i = r.nextInt( ids.size() );

		Stream stream = Stream.fetchStream( ids.get(i) );		
			result.put(stream, stream.getImages());

		GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
		Type t = new TypeToken<HashMap<Stream,List<BeastImage>>>(){}.getType();
		String json = gson.toJson(result, t);
		
		gson = new Gson();
		
		HashMap<Stream, List<BeastImage>> rec = gson.fromJson(json, t);
		
		for ( Map.Entry<Stream, List<BeastImage>> entry : rec.entrySet() ) {
			if ( Strings.isNullOrEmpty( entry.getKey().name ) ) { fail("Stream name null or empty."); }
			if ( entry.getValue().size() != 5 ) { fail("Expected 5 images, had "+ entry.getValue().size() ); }
		}
		
	}
	
	@Test
	public void testAllStreams() throws MalformedURLException {
		Map<Stream, List<BeastImage>> result = new HashMap<Stream, List<BeastImage>>();
//		Map<Stream, String> result = new HashMap<Stream, String>();
		
		
			List<Stream> streams = Stream.fetchStreams();
			Collections.sort(streams);
			for ( Stream stream : streams ) {
				result.put(stream, stream.getImages());
			}
			
		
		GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
		Type t = new TypeToken<HashMap<Stream,List<BeastImage>>>(){}.getType();
		String json = gson.toJson(result, t);
		
		gson = new Gson();
		
		HashMap<Stream, List<BeastImage>> rec = gson.fromJson(json, t);
		
		for ( Map.Entry<Stream, List<BeastImage>> entry : rec.entrySet() ) {
			if ( Strings.isNullOrEmpty( entry.getKey().name ) ) { fail("Stream name null or empty."); }
			if ( entry.getValue().size() != 5 ) { fail("Expected 5 images, had "+ entry.getValue().size() ); }
		}
		
	}
	
//	@Test
	public void fromSearchStreams() throws MalformedURLException {
		Map<Stream, List<BeastImage>> result = new HashMap<Stream, List<BeastImage>>();
//		Map<Stream, String> result = new HashMap<Stream, String>();
		

//			result = searchStreams( req.getParameter("query") );
		

		GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
		Type t = new TypeToken<HashMap<Stream,List<BeastImage>>>(){}.getType();
		String json = gson.toJson(result, t);
		
		gson = new Gson();
		
		HashMap<Stream, List<BeastImage>> rec = gson.fromJson(json, HashMap.class);
		
		for ( Map.Entry<Stream, List<BeastImage>> entry : rec.entrySet() ) {
			if ( Strings.isNullOrEmpty( entry.getKey().name ) ) { fail("Stream name null or empty."); }
			if ( entry.getValue().size() != 5 ) { fail("Expected 5 images, had "+ entry.getValue().size() ); }
		}
		
	}

	@Test
	public void test() {
		Gson gson = new Gson();
		Map<Stream, List<BeastImage>> result = new HashMap<Stream, List<BeastImage>>();
		

		List<Stream> streams = Stream.fetchStreams();
		Collections.sort(streams);
		for ( Stream stream : streams ) {
			result.put(stream, stream.getImages());
		}
		String json = gson.toJson(result);
		System.out.println(json);
	}
	
	private void makeStreams() {
		for ( int i=0; i<20; i++ ) {
			Stream s = new Stream("name"+i, "tags"+i, "owner"+i,"subscribers"+i,"welcome"+i,"url"+i);
			s.save();
			for ( int j=0; j<5; j++ ) {
				BeastImage img = new BeastImage(s.id, "owner"+i+j,"content"+i+j, "url"+i+j,
						new GeoPoint(Math.random() * Math.PI * 2, Math.acos(Math.random() * 2 - 1)));
				img.save();
			}
		}
	}

}
