package com.beastmode.api;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.beastmode.classes.BeastUser;
import com.beastmode.classes.Stream;
import com.beastmode.servlets.CreateStreamServlet;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@SuppressWarnings("serial")
public class SubscribeServletAPI extends HttpServlet {
	private static final Logger log = Logger.getLogger(CreateStreamServlet.class.getName());
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		log.info( req.getParameter("user") );
		BeastUser beast = BeastUser.fetchUser( req.getParameter("user") );
		if ( beast == null ) {
			beast = new BeastUser( req.getParameter("user"), false);
		}
		Long id = Long.parseLong( req.getParameter("streamId") );
		
		beast.subscribeTo( id );
		beast.save();		
		
		// persist to datastore

	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doGet( req, resp );
	}
}
