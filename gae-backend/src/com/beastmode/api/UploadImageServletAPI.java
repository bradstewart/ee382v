package com.beastmode.api;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.*;

import com.beastmode.classes.BeastImage;
import com.google.common.io.CharStreams;

/**
 * Send me a JSON formatted UploadedImage object.
 * 
 * @author Brad
 *
 */
@SuppressWarnings("serial")
public class UploadImageServletAPI extends HttpServlet {
	private static final Logger log = Logger.getLogger(UploadImageServletAPI.class.getName());
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doPost(req, resp);		
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		String json = CharStreams.toString( req.getReader() );
		UploadedImage img = UploadedImage.fromJson(json);
		img.uploadToBlobstore();
		BeastImage beastImg = img.toBeastImage();
		beastImg.save();

	}
}