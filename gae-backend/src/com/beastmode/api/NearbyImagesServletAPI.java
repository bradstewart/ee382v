package com.beastmode.api;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.channels.Selector;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.*;

import com.beastmode.classes.BeastImage;
import com.beastmode.classes.ImageDistanceComparator;
import com.beastmode.classes.Stream;
import com.beastmode.classes.StreamViewComparator;
import com.beastmode.util.Distance;
import com.google.appengine.api.search.GeoPoint;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@SuppressWarnings("serial")
/**
 * 
 * You're going to get a List<BeastImage> back like the other servlet.
 * Use the streamId field to build a link for the image.  
 * 
 * API Parameter		Value
 * 	location			 GeoPoint
 * 					
 * @author Brad
 *
 */
public class NearbyImagesServletAPI extends HttpServlet {
	private static final Logger log = Logger.getLogger(NearbyImagesServletAPI.class.getName());
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		String json = null;
		Gson gson = new Gson();
		double lat = Double.parseDouble(req.getParameter("lat"))/1000000;
		double lng = Double.parseDouble(req.getParameter("long"))/1000000;
		GeoPoint location = new GeoPoint(lat, lng);
		//GeoPoint location = gson.fromJson( req.getParameter("location"), GeoPoint.class );
		List<BeastImage> images = BeastImage.fetchImages();
		
		// Sort streams by distance in Km
		Comparator<BeastImage> cp = new ImageDistanceComparator( location );
		Collections.sort(images, cp);
		
//		List<BeastImage> result = new ArrayList<BeastImage>();
//		
//		String jsonSelector = req.getParameter("selector");
//		List<Stream> subsetStreams = null;
//        if ( jsonSelector == null ) {
//           subsetStreams = Stream.fetchStreams();
//        } else {
////          Gson gson = new Gson();
////          Type t = new TypeToken<Selector>(){}.getType();
////          Selector sel = gson.fromJson(jsonSelector, t);
////          subsetStreams = sel.selectStreams(streams);
//        }		

		Type t = new TypeToken<List<BeastImage>>(){}.getType();
		json = gson.toJson(images, t);
		

		resp.setContentType("application/json");
		resp.getWriter().print(json);
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doGet( req, resp );
	}
	
	private Map<Stream, List<BeastImage>> searchStreams( String query ) {
		List<Stream> allStreams = Stream.fetchStreams();
		Map<Stream, List<BeastImage>> relevantStreams = new HashMap<Stream, List<BeastImage>>();

		for( Stream stream : allStreams) {
			String allFields = stream.name + " " + stream.owner + " " + stream.tags;
			allFields = allFields.replace(",", "").replace("#", "").toLowerCase();
			ArrayList<String> fields = new ArrayList<String>();
			Collections.addAll(fields, allFields.split(" "));

			if(fields.contains(query.toLowerCase().trim()))
				relevantStreams.put(stream, stream.getImages());
		}
		
		return relevantStreams;
	}
}