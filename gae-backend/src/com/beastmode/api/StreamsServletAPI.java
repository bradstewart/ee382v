package com.beastmode.api;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.channels.Selector;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.*;

import com.beastmode.classes.BeastImage;
import com.beastmode.classes.BeastUser;
import com.beastmode.classes.Stream;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

@SuppressWarnings("serial")
/**
 * Handles stream requests for the mobile API.
 * URL Parameters:	type	all, single, geo, search		*Required
 * 					id									*Required only for "single"
 * 					location							*Required only for "geo"
 * 					query								*Required only for "search"
 * 
 * You're going to get a Map<Stream, List<BeastImage> back regardless.
 * 					
 * @author Brad
 *
 */
public class StreamsServletAPI extends HttpServlet {
	private static final Logger log = Logger.getLogger(StreamsServletAPI.class.getName());
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		String type = req.getParameter("type");
		String json = null;
		Gson gson = new Gson();
		Map<Stream, List<BeastImage>> result = new HashMap<Stream, List<BeastImage>>();
//		Map<Stream, String> result = new HashMap<Stream, String>();
		
		if ( type.equalsIgnoreCase("all") ) {
			List<Stream> streams = Stream.fetchStreams();
			Collections.sort(streams);
			for ( Stream stream : streams ) {
				result.put(stream, stream.getImages());
			}
			
		} else if ( type.equalsIgnoreCase("single") ) {
			Stream stream = Stream.fetchStream( Long.parseLong( req.getParameter("id") ) );			
			result.put(stream, stream.getImages());
			
		} else if ( type.equalsIgnoreCase("search") ) {
			result = searchStreams( req.getParameter("query") );
		} else if ( type.equalsIgnoreCase("user") ) {
			BeastUser beast = BeastUser.fetchUser( req.getParameter("userID") );
			List<Stream> streams = beast.getSubscriptionStreams();
			
			for ( Stream stream : streams ) {
				result.put(stream, stream.getImages());
			}
			
		}
		
//		String jsonSelector = req.getParameter("selector");
//		List<Stream> subsetStreams = null;
//        if ( jsonSelector == null ) {
//           subsetStreams = streams;
//        } else {
//          Gson gson = new Gson();
//          Type t = new TypeToken<Selector>(){}.getType();
//          Selector sel = gson.fromJson(jsonSelector, t);
//          subsetStreams = sel.selectStreams(streams);
//        }		
		

		
		resp.setContentType("application/json");
		GsonBuilder builder = new GsonBuilder();

        gson = builder.enableComplexMapKeySerialization().setPrettyPrinting().create();
		Type t = new TypeToken<HashMap<Stream,List<BeastImage>>>(){}.getType();
		json = gson.toJson(result, t);
		
//		System.out.println( gson.fromJson(json,t) );
		resp.getWriter().print(json);
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doGet( req, resp );
	}
	
	private Map<Stream, List<BeastImage>> searchStreams( String query ) {
		List<Stream> allStreams = Stream.fetchStreams();
		Map<Stream, List<BeastImage>> relevantStreams = new HashMap<Stream, List<BeastImage>>();

		for( Stream stream : allStreams) {
			String allFields = stream.name + " " + stream.owner + " " + stream.tags;
			allFields = allFields.replace(",", "").replace("#", "").toLowerCase();
			ArrayList<String> fields = new ArrayList<String>();
			Collections.addAll(fields, allFields.split(" "));

			if(fields.contains(query.toLowerCase().trim()))
				relevantStreams.put(stream, stream.getImages());
		}
		
		return relevantStreams;
	}
}