package com.beastmode.api;

import java.io.IOException;
import java.util.Date;

import com.beastmode.classes.BeastImage;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.files.AppEngineFile;
import com.google.appengine.api.files.FileService;
import com.google.appengine.api.files.FileServiceFactory;
import com.google.appengine.api.files.FileWriteChannel;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
import com.google.appengine.api.search.GeoPoint;
import com.google.gson.Gson;

@SuppressWarnings("deprecation")
public class UploadedImage {
	private GeoPoint location;
	private Long streamId;
	private String comments;
	private String owner;
	private String bkUrl;
	private byte[] image;

	@SuppressWarnings("unused")
	private UploadedImage() {
	}

	public UploadedImage(Long streamId, String owner, String content, byte[] image) {
		this.streamId = streamId;
		this.owner = owner;
		this.comments = content;
		this.image = image;
	}
	
	public String toJson() {
		return new Gson().toJson(this);
	}
	
	public static UploadedImage fromJson(String json) {
		Gson gson = new Gson();
		return gson.fromJson(json, UploadedImage.class);
	}
	
	public void uploadToBlobstore() throws IOException {
		FileService fileService = FileServiceFactory.getFileService();
		AppEngineFile file = fileService.createNewBlobFile("image/png");
		FileWriteChannel writeChannel = fileService.openWriteChannel(file, true);
		writeChannel.write(java.nio.ByteBuffer.wrap(this.image));
		writeChannel.closeFinally();


		// your blobKey to your data in Google App Engine BlobStore
		BlobKey blobKey = fileService.getBlobKey(file);

		// THANKS TO BLOBKEY YOU CAN GET FOR EXAMPLE SERVING URL FOR IMAGES

		// Get the image serving URL (in https:// format)
		this.bkUrl = ImagesServiceFactory.getImagesService().getServingUrl(
						ServingUrlOptions.Builder.withBlobKey(blobKey).secureUrl(true));

	}
	
	public BeastImage toBeastImage() {
		return new BeastImage(this.streamId, this.owner, this.comments, this.bkUrl, this.location);
	}



}
