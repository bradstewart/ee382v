package com.beastmode.classes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

import static com.beastmode.util.OfyService.ofy;

import com.google.common.base.Joiner;
import com.google.gson.Gson;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Serialize;
import com.googlecode.objectify.cmd.Query;


@Entity
public class Stream implements Comparable<Stream> {

	// id is set by the datastore for us
	@Id
	public Long id;
	public String name;
	public String tags;
	public String owner;
	public String subscribers;
	public String welcomeMessage;
	public Date createDate;
	public String coverImageUrl;
	@Serialize
	public Queue<Date> views;
//	private List<BeastImage> images;
  
	// TODO: figure out why this is needed
	@SuppressWarnings("unused")
	private Stream() {
	}	

	public Stream(String name, String tags, String owner, String subscribers, String welcomeMessage, String coverImageUrl) {
		this.name = name;
		this.tags = tags;
		this.owner = owner;
		this.subscribers = subscribers;
		this.welcomeMessage = welcomeMessage;
		this.coverImageUrl = coverImageUrl;
		this.createDate = new Date();
		this.views = new PriorityQueue<Date>();
	}
	
	public static Stream fetchStream( Long id ) {
		return ofy().load().type(Stream.class).id( id ).now();
	}
	
	public static List<Stream> fetchStreams( ) {
		return ofy().load().type(Stream.class).list();
	}
	
	public List<BeastImage> getImages() {
		return ofy().load().type(BeastImage.class).filter("streamId", id).list();
	}
	
	public void recordView( ) {
		Date date = new Date();
		views.add(date);
		removeOldViews(date);
	}
	
	public int getViewCount() {
		if ( views == null ) {
			System.out.println("ViewQueue is Null");
			return 0;
		} else {
			return views.size();
		}
	}
	
	public String getName() {
		return name;
	}
	
	public Long getId() {
		return id;
	}

	public void removeOldViews(Date current) {
		Long delta = 60L*60L*1000L;
		while ( current.getTime() - views.peek().getTime() > delta ) {
				views.poll();
		}
	}
	
	
	public void save() {
		ofy().save().entity(this).now();
	}
	
	public void delete() {
		ofy().delete().entity(this);
	}

	@Override
	public int compareTo(Stream other) {
		if (createDate.after(other.createDate)) {
			return -1;
		} else if (createDate.before(other.createDate)) {
			return 11;
		}
		return 0;
	}
	
	@Override
	public String toString() {
		//Joiner joiner = Joiner.on(":");
		Gson gson = new Gson();
		return gson.toJson(this);
		//return joiner.join(id.toString(), name, tags, coverImageUrl, createDate.toString());
 	}
	
	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((coverImageUrl == null) ? 0 : coverImageUrl.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		result = prime * result
				+ ((subscribers == null) ? 0 : subscribers.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Stream other = (Stream) obj;
		if (coverImageUrl == null) {
			if (other.coverImageUrl != null)
				return false;
		} else if (!coverImageUrl.equals(other.coverImageUrl))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		if (subscribers == null) {
			if (other.subscribers != null)
				return false;
		} else if (!subscribers.equals(other.subscribers))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		return true;
	}
	
}
