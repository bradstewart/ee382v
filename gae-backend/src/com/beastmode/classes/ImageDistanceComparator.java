package com.beastmode.classes;

import java.util.Comparator;

import com.beastmode.util.Distance;
import com.google.appengine.api.search.GeoPoint;

public class ImageDistanceComparator implements Comparator<BeastImage> {
	
	private GeoPoint location;
	
	public ImageDistanceComparator( GeoPoint gp ) {
		this.location = gp;
	}

	@Override
	public int compare(BeastImage arg0, BeastImage arg1) {
		return Double.compare(Distance.calculate(location, arg0.location, 'K'), 
									Distance.calculate(location, arg1.location, 'K'));
	}

}
