package com.beastmode.classes;

import static com.beastmode.util.OfyService.ofy;

import java.util.Date;
import java.util.List;

import com.google.appengine.api.search.GeoPoint;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnLoad;
import com.googlecode.objectify.annotation.OnSave;


@Entity
public class BeastImage implements Comparable<BeastImage> {

	@Id
	public Long id;
	@Index 
	public Long streamId;
	public String comments;
	public String owner;
	public String bkUrl;
	public Date createDate;
	@Ignore
	public GeoPoint location;
	private String serialLocation;
	public double lat = 0.0;
	public double lon = 0.0;

	@SuppressWarnings("unused")
	private BeastImage() {
	}

	public BeastImage(Long streamId, String owner, String content, String bkUrl) {
		this.streamId = streamId;
		this.owner = owner;
		this.bkUrl = bkUrl;
		this.comments = content;
		createDate = new Date();
	}
	
	public BeastImage(Long streamId, String owner, String content, String bkUrl, GeoPoint gp) {
		this.streamId = streamId;
		this.owner = owner;
		this.bkUrl = bkUrl;
		this.comments = content;
		this.location = gp;
		createDate = new Date();
	}
	
	public static BeastImage fetchStream( Long id ) {
		return ofy().load().type(BeastImage.class).id( id ).now();
	}
	
	public static List<BeastImage> fetchImages( ) {
		return ofy().load().type(BeastImage.class).list();
	}
	
	// Objectify does not allow GeoPoints. So we convert to and from JSON
	//  on store and load because I was too lazy to do something else.
	@OnSave
	private void serializeGeoPoint() {
		Gson gson = new Gson();
		if ( location == null) {
			location = new GeoPoint(90.0, 90.0);
		}
		lat = location.getLatitude();
		lon = location.getLongitude();
		serialLocation = gson.toJson(location, GeoPoint.class);
	}
	
//	@OnLoad
//	private void deserializeGeoPoint() {
//		Gson gson = new Gson();
//		// Added to stop annoying null pointers in data migration
//		if ( Strings.isNullOrEmpty(serialLocation) ) {
//			GeoPoint g = new GeoPoint(90.0, 90.0);
//			serialLocation = gson.toJson(g, GeoPoint.class);
//		}
//		location = gson.fromJson(serialLocation, GeoPoint.class);
//	}
	
	public void save() {
		
		ofy().save().entities(this).now();
	}

	@Override
	public String toString() {
		// Joiner is from google Guava (Java utility library), makes the toString method a little cleaner
		/*Joiner joiner = Joiner.on(":");
		System.out.println(id);
		System.out.println(streamId);
		System.out.println(bkUrl);
		System.out.println(createDate.toString());
		return joiner.join(id.toString(), streamId, comments, bkUrl==null ? "null" : bkUrl, createDate.toString());*/
		Gson gson = new Gson();
		return gson.toJson(this);
	}

	// Need this for sorting images by date
	@Override
	public int compareTo(BeastImage other) {
		if (createDate.after(other.createDate)) {
			return -1;
		} else if (createDate.before(other.createDate)) {
			return 1;
		}
		return 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bkUrl == null) ? 0 : bkUrl.hashCode());
		result = prime * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result
				+ ((createDate == null) ? 0 : createDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		result = prime * result
				+ ((streamId == null) ? 0 : streamId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BeastImage other = (BeastImage) obj;
		if (bkUrl == null) {
			if (other.bkUrl != null)
				return false;
		} else if (!bkUrl.equals(other.bkUrl))
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (createDate == null) {
			if (other.createDate != null)
				return false;
		} else if (!createDate.equals(other.createDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		if (streamId == null) {
			if (other.streamId != null)
				return false;
		} else if (!streamId.equals(other.streamId))
			return false;
		return true;
	}

}
