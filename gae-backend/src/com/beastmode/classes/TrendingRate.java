package com.beastmode.classes;

import static com.beastmode.util.OfyService.ofy;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
@Entity
public class TrendingRate {

	
		@Id
		public String id = "TrendingRate";
		public String value;
		
		@SuppressWarnings("unused")
		private TrendingRate() {
		}
		
		public TrendingRate( String value ) {
			this.value = value;
		}
		
		public void save() {
			ofy().save().entities(this).now();
		}
		
		public static TrendingRate fetchRate( ) {
			return ofy().load().type(TrendingRate.class).id( "TrendingRate" ).now();
		}

}
