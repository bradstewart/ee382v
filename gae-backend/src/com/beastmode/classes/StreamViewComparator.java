package com.beastmode.classes;

import java.util.Comparator;

public class StreamViewComparator implements Comparator<Stream> {

	@Override
	public int compare(Stream arg0, Stream arg1) {
		return -Integer.compare(arg0.getViewCount(), arg1.getViewCount());
	}

}
