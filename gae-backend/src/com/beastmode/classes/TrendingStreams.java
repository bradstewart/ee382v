package com.beastmode.classes;

import static com.beastmode.util.OfyService.ofy;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Serialize;
@Entity
public class TrendingStreams {

	
		@Id
		public String id = "TrendingStreams";
		@Serialize
		public List<Long> streams = new ArrayList<Long>();
		
		@SuppressWarnings("unused")
		public TrendingStreams() {
		}
		
		public void add( Stream stream ) {
			if ( streams == null ) {
				streams = new ArrayList<Long>();
			}
			streams.add( stream.id );
		}

		
		public void save() {
			ofy().save().entities(this).now();
		}
		
		public static List<Stream> fetchStreams( ) {
			
			List<Long> streamIds = ofy().load().type(TrendingStreams.class).id( "TrendingStreams" ).now().streams;
			List<Stream> streams = new ArrayList<Stream>();
			for ( Long id : streamIds ) {
				streams.add( Stream.fetchStream(id));
			}
			return streams;
			
			
		}

}
