package com.beastmode.util;

import com.beastmode.classes.*;
import com.googlecode.objectify.*;

public class OfyService {
	static {
		factory().register( Stream.class );
		factory().register( BeastImage.class );		
		factory().register( BeastUser.class );		
		factory().register( TrendingRate.class );
		factory().register( TrendingStreams.class );
	}
	
	public static Objectify ofy() {
	    return ObjectifyService.ofy();
	}

	public static ObjectifyFactory factory() {
	    return ObjectifyService.factory();
	}
}
