<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.beastmode.classes.Stream" %>
<%@ page import="com.beastmode.classes.BeastImage" %>
<%@ page import="com.beastmode.classes.BeastUser" %>
<%@ page import="com.beastmode.util.OfyService" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Collections" %>

<%@ page import="com.google.appengine.api.blobstore.BlobstoreService" %>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreServiceFactory" %>


<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html lang="en">
<%@include file="header.jsp" %>
	<link rel="stylesheet" type="text/css" href="css/als.css" />
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script type="text/javascript" src="js/jquery-1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src="http://codeorigin.jquery.com/ui/1.8.15/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/markerclustererplus-2.0.6/markerclusterer.min.js"></script>
	<script type="text/javascript" src="js/jquery.ui.map.js"></script>
	



<body>

<%@include file="navigation.jsp" %>
<div class="container marketing">

	<%	BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		Long streamId = new Long(request.getParameter("streamId"));
		Stream stream = Stream.fetchStream( streamId );
	%>
	
	<div style="display: none;" id="streamId"><%=streamId.toString() %></div> 

      <div class="row featurette">
        <div class="col-md-12">
          <h2 class="featurette-heading">Stream <span class="text-muted">GeoView.</span></h2>
          <p class="lead"><% out.println(stream.name); %></p>
          
          
          <div id="map_canvas3" style="width:960px;height:500px"></div>
   	<div id="inline-actions">
      <span>Max zoom level:
        <select id="zoom">
          <option value="-1">Default</option>
          <option value="7">7</option>
          <option value="8">8</option>
          <option value="9">9</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
        </select>
      </span>
      <span class="item">Cluster size:
        <select id="size">
          <option value="-1">Default</option>
          <option value="40">40</option>
          <option value="50">50</option>
          <option value="70">70</option>
          <option value="80">80</option>
        </select>
      </span>
      <span class="item">Cluster style:
        <select id="style">
          <option value="-1">Default</option>
          <option value="0">People</option>
          <option value="1">Conversation</option>
          <option value="2">Heart</option>
        </select>
      </span>
      <input id="refresh" type="button" value="Refresh Map" class="item"/>
      <a href="#" id="clear">Clear</a>
    </div>
		</div> <!-- cold-md-12 -->
		</div> <!-- roe featurette -->

<hr class="featurette-divider">
	
	
      <div class="row">
      <div class="col-md-12">
      	<p>
  			<label for="amount">Dates range:</label>
  			<input type="text" id="amount" style="border: 0; color: #f6931f; font-weight: bold;" />
		</p>
 
		<div id="slider-range"></div>
      </div>
	</div>
   		
   		<%@include file="footer.jsp" %>
	</div><!-- /container -->
			<script src="js/bootstrap.min.js"></script>
	<script src="js/custom.js"></script>
	<script type="text/javascript" src="js/map.js"></script>


  </body>
</html>
