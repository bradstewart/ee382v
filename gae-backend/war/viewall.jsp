<html lang="en">
<%@include file="header.jsp" %>
<link rel="stylesheet" type="text/css" href="css/als.css" />
<body>
<%@include file="navigation.jsp" %>

 <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">      
      <!-- START THE FEATURETTES -->



     

 <!-- Featurette Row -->
      <div class="row featurette">
        <div class="col-md-12">
          <h2 class="featurette-heading">All <span class="text-muted">Photo Streams.</span></h2>
          <p class="lead">Everything, in one place.</p>
          <div class="span12" id="display-all-streams">
		  
		</div> <!-- als-container -->
		</div> <!-- cold-md-12 -->
		</div> <!-- roe featurette -->
              

      <!-- /END THE FEATURETTES -->

    

      <!-- Example row of columns 
      
		<div class="row" id="">
		  <div class="col-md-3">
		    <div class="thumbnail">
		      <img data-src="holder.js/300x200" alt="...">
		      <div class="caption">
		        <h3>Stream Name</h3>
		        <p>Stream message</p>
		        <p><a href="#" class="btn btn-primary">View</a></p>
		      </div>
		    </div>
		  </div>
		  
		</div> -->
		
		
	
    <%@include file="footer.jsp" %>
    </div> <!-- Marketing Container -->
	<%@include file="javascript.jsp" %>
			<script type="text/javascript" src="js/jquery.als-1.2.min.js"></script>
	
    <script>
	//AJAX handler to display content for "viewall.html"
	$(document).ready( function() {	
		$('ul.nav li').removeClass("active");
		$('#nav-view').addClass("active");
		
		
		var posting = $.post('/viewall');
		posting.done( function( data ) {
			$("#display-all-streams").html( data );
			/*$("#all-streams").als({
				visible_items: 4,
				scrolling_items: 1,
				orientation: "horizontal",
				circular: "yes",
				autoscroll: "no",
				
			});*/
		});
		
		var postTrending = $.post('/trending');
		postTrending.done( function( data ) {
			$("#display-trending-streams").html( data );
			/* $("#trending-streams").als({
				visible_items: 3,
				scrolling_items: 1,
				orientation: "horizontal",
				circular: "yes",
				autoscroll: "no",
				start_from: 1
			}); */
		});		
	});
	</script>
		    
  </body>
</html>
