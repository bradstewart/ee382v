<html lang="en">
<%@include file="header.jsp" %>
<link rel="stylesheet" type="text/css" href="css/als.css" />
<body>
<%@include file="navigation.jsp" %>

 <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">      
      <!-- START THE FEATURETTES -->
	 <div class="row featurette">
        <div class="col-md-6">
          <h2 class="featurette-heading">Welcome <span class="text-muted">to Beast Mode.</span></h2>
          <p class="lead">Awesome, simple image sharing.</p>
		  <p> Create <span class="text-muted">streams, upload images, and share them to express your inner beastliness.</span>
		  Explore <span class="text-muted">public streams to find awesome new images to become even more beast-like.</span> 
		  There's nothing left to say.</p>

		</div> <!-- cold-md-12 -->
		<div class="col-md-6 well">
          <h2 class="featurette-heading">Login <span class="text-muted">With Google.</span></h2>
          <p class="lead">To unlock advanced features like stream management and subscriptions, login with a Google account.</p>
		  <form action="<%=userService.createLoginURL(request.getRequestURI())%>">
			<button type="submit" class="btn btn-primary">Login with Google</button>
		  </form>

		</div> <!-- cold-md-12 -->
	  </div> <!-- roe featurette -->
	  
	        

	<hr class="featurette-divider">
	
      <div class="row featurette">
        <div class="col-md-12">
         <h2 class="featurette-heading">Trending <span class="text-muted">Photo Streams.</span></h2>
          <p class="lead">The most popular images and streams.</p>
        <div class="als-container" id="trending-streams">
  <span class="als-prev"></span>
  <div class="als-viewport">	
	<div class="als-wrapper" id="display-trending-streams">
		
	</div>
	</div>
  <span class="als-next"></span>
</div> <!-- als-container -->
</div> <!-- col-md-12 -->

</div> <!-- Featurette Row -->
      
              
      

     
      

      <!-- /END THE FEATURETTES -->
		
	
    <%@include file="footer.jsp" %>
    </div> <!-- Marketing Container -->
	<%@include file="javascript.jsp" %>
			<script type="text/javascript" src="js/jquery.als-1.2.min.js"></script>
	
    <script>
	//AJAX handler to display content for "viewall.html"
	$(document).ready( function() {	
		$('ul.nav li').removeClass("active");
		$('#nav-home').addClass("active");
		
		
			
		var postTrending = $.post('/trending');
		postTrending.done( function( data ) {
			$("#display-trending-streams").html( data );
			$("#trending-streams").als({
				visible_items: 3,
				scrolling_items: 0,
				orientation: "horizontal",
				circular: "yes",
				autoscroll: "no",
				start_from: 1
			});
		});		
	});
	</script>
		    
  </body>
</html>
