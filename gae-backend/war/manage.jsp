<%@ page import="com.beastmode.classes.Stream"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.Arrays"%>
<html lang="en">
<%@include file="header.jsp"%>
<body>
	<%@include file="navigation.jsp"%>
	<div class="container marketing">

		<!-- Streams I own -->
		<h2 class="featurette-heading">
			Streams <span class="text-muted">I Own.</span>
		</h2>

		<table id="ownedStreams" class="table table-hover tablesorter">
			<thead>
				<tr>
					<th>Name</th>
					<th>Last New Picture</th>
					<th>Number of Pictures</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
				<%
					List<Stream> ownedStreams = beast.getOwnedStreams();
				if ( ownedStreams != null ) {
					for (Stream s : ownedStreams) {
						if (s.owner != null && s.owner.equals(user.getNickname())) {
							Date mostRecent;
							if (s.getImages().size() > 0) {
								mostRecent = s.getImages().get(0).createDate;
							} else {
								mostRecent = s.createDate;
							}
				%>
				<tr>
					<td><%=s.getName()%></td>
					<td><%=mostRecent%></td>
					<td><%=s.getImages().size()%></td>
					<td><input type="checkbox" class="checkbox" value="<%=s.getId()%>" name="owned"></td>
				</tr>
				<%
					}
				  }
				}
				%>
			</tbody>
		</table>
		<form id="delete-form" action="/delete?user=<% out.print(user.getNickname() ); %>&" >
			<input type="hidden" name="user" value="<% out.print(user.getNickname() ); %>">
			<button type="submit" class="btn btn-primary" id="delete_owned">Delete selected streams</button>
		</form>
		<!-- Streams I subscribe to -->
		<hr class="featurette-divider">


		<h2 class="featurette-heading">
			Streams <span class="text-muted">I Subscribe To.</span>
		</h2>



		<table id="subscribedStreams" class="table table-hover tablesorter">
			<thead>
				<tr>
					<th>Name</th>
					<th>Last New Picture</th>
					<th>Number of Pictures</th>
					<th>Views</th>
					<th>Unsubscribe</th>
				</tr>
			</thead>
			<tbody>
				<%
					//List<Stream> subscribedStreams = Stream.fetchStreams();
				    List<Stream> subscribedStreams = beast.getSubscriptionStreams();
				    if ( subscribedStreams != null ) {
					for (Stream s : subscribedStreams) {
						String mostRecent;
					
						if (s.getImages().size() > 0) {
							mostRecent = s.getImages().get(0).createDate.toString();
						} else {
							mostRecent = "No pictures.";
						}
					
						
						/* if (s.subscribers != null) {
							List<String> subscribers = Arrays.asList(s.subscribers
									.split("\\s*,\\s*"));
							if (subscribers.contains(user.getNickname())) {
								Date mostRecent;
								if (s.getImages().size() > 0)
									mostRecent = s.getImages().get(0).createDate;
								else
									mostRecent = s.createDate; */
				%>
				<tr>
					<td><%=s.getName()%></td>
					<td><%=mostRecent%></td>
					<td><%=s.getImages().size()%></td>
					<td><%=s.getViewCount()%></td>
					<td><input type="checkbox" class="checkbox2"
						value=<%=s.getId()%> name="subscribed"></td>
				</tr>
				<%
					}
						
				}
					
				%>
			</tbody>
		</table>
		<form id="unsubscribe-form" action="/unsubscribe?user=<% out.print(user.getNickname() ); %>&" >
			<input type="hidden" name="user" value="<% out.print(user.getNickname() ); %>">
			<button type="submit" class="btn btn-primary" id="delete_subscribed">Unsubscribe from selected streams</button>
		</form>

		<%@include file="footer.jsp"%>
	</div>
	<!-- /container -->
	<%@include file="javascript.jsp"%>

	<script src="js/jquery.tablesorter.js"></script>
	<script>
		
		$(document).ready(function() 
    	{ 
			$('ul.nav li').removeClass("active");
			$('#nav-manage').addClass("active");
			
        	$("#ownedStreams").tablesorter(); 
        	$("#subscribedStreams").tablesorter(); 
    		$("#delete_owned").hide();
    		$("#delete_subscribed").hide();
    	}); 
		
		var owned = 0;
		var subscribed = 0;
		var selectedSubscribed = new Array();
		
		$('.checkbox').change(function() {
		    if ($(this).is(':checked')) {
		        $('#delete_owned').show(400);
		        owned += 1;
		    }
		    else {
		    	owned -= 1;
		    	if(owned == 0)
		       		$('#delete_owned').hide(400);
		    }
		});
		$('.checkbox2').change(function() {
		    if ($(this).is(':checked')) {
		        $('#delete_subscribed').show(400);
		        subscribed += 1;
		    }
		    else {
		        subscribed -= 1;
		        if(subscribed == 0)
		        	$('#delete_subscribed').hide(400);
		    }
		});
		
		
		
		$("#delete_owned").click( function(e)
           {
           	 var selectedOwned = new Array();
             $("input:checkbox[name=owned]:checked").each(function() {
       			selectedOwned.push($(this).val());
       			$(this).parent().parent().remove();
       	      
  			 });
             e.preventDefault();
             
             var url = url = $('#delete-form').attr( 'action' );
             alert(selectedOwned);
             var out = JSON.stringify(selectedOwned);
             //url = url+"params="+out;
             /* Send the data using post */
             	var posting = $.post( url,
             			{
           	 	 			params: out
             			}
           	 	);
            
             /* Put the results in a div
             		You've got json coming back at you with
             		all the fields from a mentor in the matches
             		field, so process and insert into HTML as you wish */
             	posting.done(function( data ) {
             		$('#delete_owned').hide(400);
             	});
           }
        );
        $("#delete_subscribed").click( function(e)
           {
           	 var selectedSubscribed = new Array();
             $("input:checkbox[name=subscribed]:checked").each(function() {
       			selectedSubscribed.push($(this).val());
       			$(this).parent().parent().remove();
      
  			 });
             e.preventDefault();
             //var user = <% out.print(user.getEmail()); %>;
             var url = url = $('#unsubscribe-form').attr( 'action' );
             var out = JSON.stringify(selectedSubscribed);
             //url = url+"params="+out;
             /* Send the data using post */
             	var posting = $.post( url,
             			{
           	 	 			params: out
             			}
           	 	);
            
             /* Put the results in a div
             		You've got json coming back at you with
             		all the fields from a mentor in the matches
             		field, so process and insert into HTML as you wish */
             	posting.done(function( data ) {
             		$('#delete_subscribed').hide(400);
             	});
           }
        );
	</script>

</body>
</html>