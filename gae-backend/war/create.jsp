<html lang="en">
<%@include file="header.jsp" %>
<body>

<%@include file="navigation.jsp" %>

	<div class="container marketing">
      <!-- Example row of columns -->
      <div class="row featurette">
        <div class="col-lg-8">
        <h2 class="featurette-heading">Create <span class="text-muted">a Photo Stream.</span></h2>
          <p class="lead">Enter the information below and press Create Stream.</p>
          

		<form class="form-horizontal" name="createStreamInput" action="createstream">
		  <div class="form-group">
		    <label for="inputName" class="col-lg-2 control-label">Name your stream</label>
		    <div class="col-lg-10">
		      <input type="text" name="name" class="form-control" id="inputName" placeholder="Name">
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputSubscribers" class="col-lg-2 control-label">Add subscribers</label>
		    <div class="col-lg-10">
		      <textarea rows="3" name="subscribers" class="form-control" id="inputSubscribers" placeholder="bob@example.com, jim@example.com, ..."></textarea>
		         	
		      <textarea rows="3" name="message" class="form-control" id="inputSubscribers" placeholder="Optional message for invite"></textarea>
		      <span class="help-block">Separate email addresses with commas. Welcome message is optional.</span> 
		    </div>
		  </div>
		
		  <div class="form-group">
		    <label for="inputTags" class="col-lg-2 control-label">Tag your stream</label>
		    <div class="col-lg-10">
		      <textarea rows="3" name="tags" class="form-control" id="inputTags" placeholder="#awesome #382v #beast #..."></textarea>
		         	
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputCoverURL" class="col-lg-2 control-label">URL to cover image</label>
		    <div class="col-lg-10">
		      <input type="text" name="cover" class="form-control" id="inputCoverURL" placeholder="http://www.flickr.com/image.jpg">
		      <span class="help-block">Can be empty.</span>
		    </div>
		  </div>
		  
		  <div class="form-group">
		    <div class="col-lg-offset-2 col-lg-10">
		      <input type="hidden" name="creator" value="<% out.print(user.getNickname()); %>">
		      <button type="submit" class="btn btn-primary">Create Stream</button>
		    </div>
		  </div>
		</form>
		
		</div> <!-- col-lg-12 -->
		</div> <!-- row -->
          
      	<%@include file="footer.jsp" %>
	</div><!-- /container -->
	<%@include file="javascript.jsp" %>
	<script>
	$(document).ready( function() {
		$('ul.nav li').removeClass("active");
		$('#nav-create').addClass("active");
	});
	</script>
  </body>
</html>
