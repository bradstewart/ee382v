<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page import="java.util.List"%>
<%@ page import="com.beastmode.classes.BeastUser"%>
<%@ page import="com.google.appengine.api.users.User"%>
<%@ page import="com.google.appengine.api.users.UserService"%>
<%@ page import="com.google.appengine.api.users.UserServiceFactory"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

	<%
	UserService userService = UserServiceFactory.getUserService();
	User user = userService.getCurrentUser();
	BeastUser beast;
	if (user != null) {
		
		
		pageContext.setAttribute("user", user);
		//String redirectURL = "/manage.jsp";
	    //response.sendRedirect(redirectURL);
	} else {
		user = new User("Anonymous", "anon", "Anonymous");
			//String redirectURL = "/login.jsp";
			//response.sendRedirect(redirectURL);
	}
	
	beast = BeastUser.fetchUser(user.getNickname());
	if (beast == null)
	{
		beast = new BeastUser(user.getNickname(), false);
		beast.save();
	}
	
	%>

	<nav class="navbar navbar-default" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="#">Beast Mode</a>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      <li id="nav-home"><a href="index.jsp">Home</a></li>
      <li id="nav-manage"><a href="manage.jsp">Manage</a></li>
      <li id="nav-create"><a href="create.jsp">Create</a></li>
      <li id="nav-view"><a href="viewall.jsp">View</a></li>
      <li id="nav-trending"><a href="trending.jsp">Trending</a></li>
      <li id="nav-social"><a href="social.jsp">Social</a></li>
    </ul>
    <form class="navbar-form navbar-right" role="search" action="/searchresults.jsp" method="post">
      <div class="form-group">
        <input type="text" class="form-control" id="autocomplete-search" placeholder="Search" name="query">
      </div>
      <button type="submit" class="btn btn-default" >Submit</button>
    </form>
     <% if ( user.getUserId().equals("Anonymous") ) { %>
      	<div class="navbar-right">
      	<ul class="nav navbar-nav">
      	
 			<li><a href="<%=userService.createLoginURL(request.getRequestURI())%>">Sign	in</a></li>
		</ul>
	</div>


    <% } else { %>
          <div class="navbar-right">
          	<ul class="nav navbar-nav">
          	<li><p class="navbar-text">Signed in as ${fn:escapeXml(user.nickname)}</p></li>			
			<li><a	href="<%=userService.createLogoutURL("/")%>">Logout</a></li>
			</ul>	
          </div>
          <% } %>
  </div><!-- /.navbar-collapse -->
</nav>	
	<!-- NAVBAR
================================================== 
  
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">Beast Mode</a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="manage.jsp">Home</a></li>
                <li><a href="create.jsp">Create</a></li>
                <li><a href="viewall.jsp">View</a></li>
                
              </ul>
              
              
          <% if ( user != null && !user.getEmail().equals("Anonymous") ) { %>
          <div class="navbar-right">
          	<ul class="nav navbar-nav">
          	<li><p class="navbar-text">Signed in as ${fn:escapeXml(user.nickname)}</p></li>			
			<li><a	href="<%=userService.createLogoutURL(request.getRequestURI())%>">Logout</a></li>
			</ul>	
          </div>
          <% } %>
          
            </div>
          </div>
        </div>

      </div>
    </div> -->