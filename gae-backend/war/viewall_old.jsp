<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ page import="com.beastmode.classes.Stream" %>
<%@ page import="com.beastmode.util.OfyService" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Collections" %>



<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Justified Nav Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/justified-nav.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">

      <div class="masthead">
        <h3 class="text-muted">Beast Mode</h3>
        <ul class="nav nav-justified">
          <li><a href="#">Manage</a></li>
          <li><a href="#">Create</a></li>
          <li class="active"><a href="#">View</a></li>
          <li><a href="#">Search</a></li>
          <li><a href="#">Trending</a></li>
          <li><a href="#">Social</a></li>
        </ul>
      </div>

      <!-- Jumbotron -->
      <div class="jumbotron">
        <h1>Viewing All Streams</h1>
        
      </div>

      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-3">
			<p>Image one</p>
		</div>
		<div class="col-md-3">
			<p>Image two</p>
		</div>
		<div class="row">
			<table>
			<%
					List<Stream> th = OfyService.ofy().load().type(Stream.class).list();
					Collections.sort(th);
					for (Stream s : th ) {
					  // APT: calls to System.out.println go to the console, calls to out.println go to the html returned to browser
					  // the line below is useful when debugging (jsp or servlet)
					  System.out.println("s = " + s);
					  %>
					  <tr><td> <img width="100" height="100" src="<%= s.coverImageUrl %>"> </td> 
					  <td><a href="ShowStream.jsp?streamId=<%= s.id %>&streamName=<%= s.name %>"> <%= s.name %></a></td><tr>
			
			<% } %>
    		</table>
		</div>
          
      <!-- Site footer -->
      <div class="footer">
        <p>&copy; Beast Mode 2013</p>
      </div>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
    <!-- Placed at the end of the document so the pages load faster -->
  </body>
</html>
