<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Collections"%>
<%@ page import="com.beastmode.classes.Stream"%>
<%@ page import="com.google.appengine.api.users.User"%>
<%@ page import="com.google.appengine.api.users.UserService"%>
<%@ page import="com.google.appengine.api.users.UserServiceFactory"%>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<title>Search Results</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-theme.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="css/justified-nav.css" rel="stylesheet">
</head>
<%@include file="header.jsp"%>
<body>
	<%@include file="navigation.jsp"%>
	<div class="container marketing">
		<h2 class="featurette-heading">
			Search <span class="text-muted">Results</span>
		</h2>
	<%
	List<Stream> allStreams = Stream.fetchStreams();
		List<Stream> relevantStreams = new ArrayList<Stream>();

		String query = request.getParameter("query");
		for( Stream stream : allStreams) {
			String allFields = stream.name + " " + stream.owner + " " + stream.tags;
			allFields = allFields.replace(",", "").replace("#", "").toLowerCase();
			ArrayList<String> fields = new ArrayList<String>();

			Collections.addAll(fields, allFields.split(" "));

			if(fields.contains(query.toLowerCase().trim()))
				relevantStreams.add(stream);
		}

		StringBuilder sb = new StringBuilder();
		if(relevantStreams != null)
			for ( Stream stream : relevantStreams ) {			
				sb.append("<br><div class=\"span3\"style=\"width: 250px;\"><div class=\"thumbnail\">");
				sb.append("<img style=\"height: 200px;width: 200px;\" src=\""+stream.coverImageUrl+"\"> ");
				sb.append("<a href=\""+stream.coverImageUrl+"\"></a>");
				sb.append("<div class=\"caption\" align=\"center\">");
				sb.append("<h3>"+stream.name+"</h3>");
				sb.append("<p>"+stream.tags+"</p>");
				String url = "/viewstream.jsp?streamId="+stream.id;
				sb.append("<p><a href=\""+url+"\" class=\"btn btn-primary\">View</a></p>");
				sb.append("</div></div></div><br>");
			}
		if(sb.toString().length() == 0)
			out.print("<h3>No Results</h3>");
		else
			out.print(sb.toString());
			%>
	<%@include file="footer.jsp"%>

	</div></div>
	<%@include file="javascript.jsp"%>

<script>
  $(document).ready(function() 
    { 
		$('ul.nav li').removeClass("active");
	});
</script>

</body>
</html>