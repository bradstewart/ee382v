<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page import="java.util.List"%>
<%@ page import="com.beastmode.classes.BeastUser"%>
<%@ page import="com.google.appengine.api.users.User"%>
<%@ page import="com.google.appengine.api.users.UserService"%>
<%@ page import="com.google.appengine.api.users.UserServiceFactory"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>

<body>

	<%
		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();
		if (user != null) {
			BeastUser s = BeastUser.fetchUser(user.getNickname());
			if (s == null)
			{
				s = new BeastUser(user.getNickname(), false);
				s.save();
			}
			
			pageContext.setAttribute("user", user);
			String redirectURL = "/manage.jsp";
		    response.sendRedirect(redirectURL);
		} else {
	%>
	<p>
		<h1>Welcome to Beast Mode!</h1>
		<h2>Share the world!</h2>
		<a href="<%=userService.createLoginURL(request.getRequestURI())%>">Sign
		in</a> using Gmail to begin sharing!
		
	</p>
	<%
		}
	%>
	

</body>
</html>