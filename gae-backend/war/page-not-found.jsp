<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page import="java.util.List"%>
<%@ page import="com.google.appengine.api.users.User"%>
<%@ page import="com.google.appengine.api.users.UserService"%>
<%@ page import="com.google.appengine.api.users.UserServiceFactory"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<title>404</title>

<!-- Bootstrap core CSS -->
<link href="/css/bootstrap.css" rel="stylesheet">
<link href="/css/bootstrap-theme.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="/css/justified-nav.css" rel="stylesheet">
</head>
<%@include file="header.jsp"%>
<body>
	<%@include file="navigation.jsp"%>
	<div class="container marketing">
		<h2 class="featurette-heading">Page Not Found</h2>

	<%@include file="footer.jsp"%>

	</div></div>
	<%@include file="javascript.jsp"%>

<script>
  $(document).ready(function() 
    { 
		$('ul.nav li').removeClass("active");
	});
</script>

</body>
</html>