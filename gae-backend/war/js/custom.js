
$(document).ready( function() {
	// Controls toggling the nav buttons
	$('div.masthead ul li').on('click', function() {
		$('div.masthead ul li.active').removeClass( 'active' );
		$(this).toggleClass('active');		
	});
	
	// autocomplete functions for the search box
	var cache = {};
    $( "#autocomplete-search" ).autocomplete({
      minLength: 2,
      source: function( request, response ) {
        var term = request.term;
        if ( term in cache ) {
          response( cache[ term ] );
          return;
        }
 
        $.getJSON( "/autocomplete.json", request, function( data, status, xhr ) {
          cache[ term ] = data;
          response( data );
        });
      }
    });
});

