// JavaScript functions for drawing the clustered map of streams
// EE 382V Miniproject Phase 3

$(document).ready( function() {
	initialize();
	initializeSlider();
	// We need to bind the map with the "init" event otherwise bounds will be null
	/*$('#map_canvas').gmap({'zoom': 2, 'disableDefaultUI':true}).bind('init', function(evt, map) { 
		var bounds = map.getBounds();
		var southWest = bounds.getSouthWest();
		var northEast = bounds.getNorthEast();
		var lngSpan = northEast.lng() - southWest.lng();
		var latSpan = northEast.lat() - southWest.lat();

		for ( var i = 0; i < 1000; i++ ) {
			$(this).gmap('addMarker', { 'position': new google.maps.LatLng(southWest.lat() + latSpan * Math.random(), southWest.lng() + lngSpan * Math.random()) } ).click(function() {
				$('#map_canvas').gmap('openInfoWindow', { content : 'Hello world!' }, this);
			});
		}
		$(this).gmap('set', 'MarkerClusterer', new MarkerClusterer(map, $(this).gmap('get', 'markers')));
	});
	
	$('#map_canvas2').gmap({'zoom': 2, 'disableDefaultUI':true}).bind('init', function(evt, map) { 
		// This URL won't work on your localhost, so you need to change it
		// see http://en.wikipedia.org/wiki/Same_origin_policy
		$.getJSON( 'json/data.json', function(data) { 
			$.each( data.images, function(i, marker) {
				$('#map_canvas2').gmap('addMarker', { 
					'position': new google.maps.LatLng(marker.lat, marker.lon), 
					'bounds': true 
				}).click(function() {
					$('#map_canvas2').gmap('openInfoWindow', { 'content': marker.owner }, this);
				});
			});
		});
		
		$('#map_canvas2').gmap('set', 'MarkerClusterer', new MarkerClusterer(map, $(this).gmap('get', 'markers')));
	});*/
	
});
	  var streamId = null;
	  var markerClusterer = null;
      var map = null;
      var dataUrl = '/stream.json?streamId=';
      var imageUrl = 'http://chart.apis.google.com/chart?cht=mm&chs=24x32&' +
          'chco=FFFFFF,008CFF,000000&ext=.png';

    //  google.maps.event.addDomListener(window, 'load', initialize);

 
        
        // Generate random locations for test purposes
      function generateLatLng() {
	  	var bounds = map.getBounds();
		var southWest = bounds.getSouthWest();
		var northEast = bounds.getNorthEast();
		var lngSpan = northEast.lng() - southWest.lng();
		var latSpan = northEast.lat() - southWest.lat();
		var lat = southWest.lat() + latSpan * Math.random();
		var lng = southWest.lng() + lngSpan * Math.random();
		var location = new google.maps.LatLng(lat, lng);
		return location;
      }
        

        var markers = [];
        var today = new Date();
    	var startMonth = today.getMonth() - 6;
    	var start = new Date(today.getFullYear(), startMonth, today.getDate());
    	  
     function refreshMap() {
         if (markerClusterer) {
           markerClusterer.clearMarkers();
         }  

        var markerImage = new google.maps.MarkerImage(imageUrl,
          new google.maps.Size(24, 32));

        $.getJSON( dataUrl+streamId, function(data) { 
			$.each( data.images, function(i, image) {
				var location = null;
				if ( image.lat == '0.0' ) {
					location = generateLatLng();
				} else {
					location = new google.maps.LatLng( image.lat, image.lon );
				}
				var marker = new google.maps.Marker({ 
					position: location,
					date: image.createDate,
					icon: markerImage
				});	
				var img = '<p><img src="'+image.bkUrl+'=s100'+'"></p>';
                var text = '<p>STUFF</p>';
                var infowindow = new google.maps.InfoWindow({
                	content: img
                });
                google.maps.event.addListener(marker, 'mouseover', function() {
                    infowindow.open(map, marker);
                });
				markers.push(marker);
			});
       

	        var zoom = parseInt(document.getElementById('zoom').value, 10);
	        var size = parseInt(document.getElementById('size').value, 10);
	        var style = parseInt(document.getElementById('style').value, 10);
	        zoom = zoom == -1 ? null : zoom;
	        size = size == -1 ? null : size;
	        style = style == -1 ? null: style;
	
	        markerClusterer = new MarkerClusterer(map, markers, {
	          maxZoom: zoom,
	          gridSize: size
	        });
        }); // end getJSON
     }
        

      function initializeSlider() {
    	  var diff =  Math.floor(( today - start ) / 86400000);
    	  console.log(diff);
    	  //console.log(diff);
	      $( "#slider-range" ).slider({
	          range: true,
	          min: 0,
	          max: diff+20,
	          values: [ 0+20, diff ],
	          slide: function( event, ui ) {
	        	  var startString = getDateString(ui.values[ 0 ]);
	        	  var endString = getDateString(ui.values[ 1 ]);
	              $( "#amount" ).val( startString + " - " + endString );
	              filterByDate(ui);
	            }
	        });
	        
	        $( "#amount" ).val( start.getMonth()+'/'+start.getDate()+'/'+start.getFullYear() +
	          " - " + today.getMonth()+'/'+today.getDate()+'/'+today.getFullYear() );
	      
      }
      
      function getDateString( days ) {
    	  days += start.getDate();
    	  var months = start.getMonth();
    	  while ( days > 30 ) {
    		  months += 1;
    		  days -= 30;
    	  }
    	      	  
    	  return months+'/'+days+'/'+start.getFullYear();
      }
      
      function filterByDate(ui) {
      	var left = ui.values[ 0 ];
      	var right = ui.values[ 1 ];
     	  markers.forEach( function(marker) {
     		 var markerDate = new Date(marker.date);
     		 var diff =  Math.floor(( markerDate - start ) / 86400000);
     		 if ( diff > left && diff < right ) {
     			 marker.setVisible(true);
     		 } else {
     			 marker.setVisible(false);
     		 }
     		  
     	  });     	  
     	  
        }
      
      
      function initialize() {
    	  streamId = $('#streamId').text();
    	  
          map = new google.maps.Map(document.getElementById('map_canvas3'), {
            zoom: 2,
            center: new google.maps.LatLng(39.91, 116.38),
            mapTypeId: google.maps.MapTypeId.ROADMAP
          });
          


          $('#refresh').on('click', function() {
        	  refreshMap();
          });

          
          $('#clear').on('click', function(e) {
        	  clearClusters(e);
          });


          refreshMap();
        }

    function clearClusters(e) {
      e.preventDefault();
      e.stopPropagation();
      markerClusterer.clearMarkers();
    }




