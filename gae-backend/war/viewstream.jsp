<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.beastmode.classes.Stream" %>
<%@ page import="com.beastmode.classes.BeastImage" %>
<%@ page import="com.beastmode.classes.BeastUser" %>
<%@ page import="com.beastmode.util.OfyService" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Collections" %>

<%@ page import="com.google.appengine.api.blobstore.BlobstoreService" %>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreServiceFactory" %>


<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html lang="en">
<%@include file="header.jsp" %>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="/css/als.css" />
<link rel="stylesheet" href="/css/style.css">
<!-- blueimp Gallery styles -->
<link rel="stylesheet" href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="/css/jquery.fileupload.css">
<link rel="stylesheet" href="/css/jquery.fileupload-ui.css">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="/css/jquery.fileupload-ui-noscript.css"></noscript>

<body>

<%@include file="navigation.jsp" %>
<div class="container marketing">

<%		BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		Long streamId = new Long(request.getParameter("streamId"));
		//String streamName = request.getParameter("streamName");
		//out.println(streamName);
		Stream stream = Stream.fetchStream( streamId );
		stream.recordView(); 
		List<BeastImage> images = stream.getImages();
		stream.save();
		Collections.sort(images);
		%>

      <div class="row featurette">
        <div class="col-md-12">
          <h2 class="featurette-heading">Photo<span class="text-muted">Stream.</span></h2>
          <p class="lead"><% out.println(stream.name); %></p>
 
          <div class="als-container" id="all-streams">
		  <span class="als-prev">
		  <% if ( images.size() > 4) { %>
		  <img src="images/thin_left_arrow_333.png" alt="prev" title="previous" />
		  <% } %>
		  </span>
		  <div class="als-viewport">	
			<div class="als-wrapper">
      
    
		<%
				
				StringBuilder sb = new StringBuilder();
				for ( BeastImage img : images ) {
					sb.append("<div class=\"als-item\"><div class=\"thumbnail\">");
					sb.append("<img style=\"height: 200px;width: 200px;\" src=\""+img.bkUrl+"\"> ");
					sb.append("<a href=\""+img.bkUrl+"\"></a>");
					sb.append("<div class=\"caption\">");
					sb.append("<p>"+img.comments+"</p>");
		//			String url = "/view?streamId="+stream.id;
		//			sb.append("<p><a href=\""+img.bkUrl+"\" class=\"btn btn-primary\"></a></p>");
					sb.append("</div></div></div>");
		//     		  out.println("<img src=\"" + img.bkUrl + "\">"); // better to not use println for html output, use templating instead
		     		}
				out.println(sb.toString());
		%>
	</div>
			</div>
			 <span class="als-next">
 		  <% if ( images.size() > 4) { %>
			 
			 <img src="images/thin_right_arrow_333.png" alt="next" title="next" />
		 <% } %>
			 </span>
		</div> <!-- als-container -->
		</div> <!-- cold-md-12 -->
		</div> <!-- roe featurette -->

<!-- APT: note how we are adding additional parameters when we create the uploadurl - this way blobstore service
     can pass them on to the upload servlet, so upload knows which stream the image blob corresponds to -->

<!-- Example row of columns -->
<hr class="featurette-divider">
	
	
      <div class="row">
		<h3>Upload Image</h3>
		<form id="fileupload" action="<%= blobstoreService.createUploadUrl("/upload?streamId=" 
			+ streamId+"&user="+user.getNickname()) %>" method = "post" enctype="multipart/form-data">
		 <!-- <div class="form-group">
		    <label for="inputFile" class="col-lg-2 control-label">Upload</label>
		    <div class="col-lg-10">
				<input type="file" name="file" id="inputFile" class="form-control" placeholder="File"> 		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputComments" class="col-lg-2 control-label">Comments</label>
		    <div class="col-lg-10">
		      <textarea rows="3" name="comments" class="form-control" id="inputComments" placeholder="Comments"></textarea>  	
		    </div>
		  </div>
		  		  
		  <div class="form-group">
		    <div class="col-lg-offset-2 col-lg-10">
		      <button type="submit" class="btn btn-primary">Upload</button>
		    </div>
		  
		  </div>-->
		  <!-- Redirect browsers with JavaScript disabled to the origin page -->
        <noscript><input type="hidden" name="redirect" value="http://blueimp.github.io/jQuery-File-Upload/"></noscript>
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
            <div class="col-lg-7">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add files...</span>
                    <input type="file" name="files" multiple>
                </span>
                <button type="submit" class="btn btn-primary start">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start upload</span>
                </button>
                <button type="reset" class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel upload</span>
                </button>
                <button type="button" class="btn btn-danger delete">
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" class="toggle">
                <!-- The global file processing state -->
                <span class="fileupload-process"></span>
            </div>
            <!-- The global progress state -->
            <div class="col-lg-5 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
		</form>
		    <br>
		<!-- The blueimp Gallery widget -->
		<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
		    <div class="slides"></div>
		    <h3 class="title"></h3>
		    <a class="prev">‹</a>
		    <a class="next">›</a>
		    <a class="close">×</a>
		    <a class="play-pause"></a>
		    <ol class="indicator"></ol>
	</div>
	</div>
	<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
	
	
			<div class="row">
		<div class="well col-lg-6">
			<h3>Subscribe to Stream</h3>
			<form action="/subscribe" method = "post" >
			<input type="hidden" name="streamId" value="<%= streamId %>">
			<input type="hidden" name="user" value="<% out.print( user.getNickname() ); %>"> 	
	
			<div class="form-group">
			<label for="sub-btn" class="col-lg-2 control-label">Subscribe</label>
			    <div class="col-lg-10">
			    
			    <% if ( user.getEmail().equals("Anonymous") ) { %>
			    	<button id="sub-btn" type="submit" class="btn btn-primary disabled">Not logged in</button>
			    <% } else if ( BeastUser.fetchUser( user.getNickname() ).isSubscribedTo( streamId ) ) { %>
			      <button type="submit" class="btn btn-primary disabled">Subscribed</button>
			      <% } else { %>
			      <button id="sub-btn" type="submit" class="btn btn-primary ">Subscribe</button>
			      <% } %>
			    </div>		  
		 	 </div>
			</form>
		</div>
		
		</div>
    		<button type="submit" class="btn btn-primary" id="facebook-post" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href), 
      		'facebook-share-dialog', 'width=626,height=436'); return false;">Post to Facebook</button>
 
	<%@include file="footer.jsp" %>
	</div><!-- /container -->
	<%@include file="javascript.jsp" %>
    <script type="text/javascript" src="/js/jquery.als-1.2.min.js"></script>
	
    <script>
	//AJAX handler to display content for "viewall.html"
	$(document).ready( function() {	
		    $("#facebook-post").hide();
			  <% if ( images.size() > 4) { %>

			$("#all-streams").als({
				visible_items: 4,
				scrolling_items: 1,
				orientation: "horizontal",
				circular: "yes",
				autoscroll: "no"
				
			});
			<% } %>
			
			$('#fileupload').fileupload({
		        dataType: 'json',
		        done: function (e, data) {
		            $.each(data.result.files, function (index, file) {
		                $('<p/>').text(file.name).appendTo(document.body);
		            });
		        }
		    });
		});
	</script>
	<script>
  window.fbAsyncInit = function() {
  FB.init({
    appId      : '555252304523027', // App ID
    channelUrl : '//www.ericissunny.appspot.com/channel.html', // Channel File
    status     : true, // check login status
    cookie     : true, // enable cookies to allow the server to access the session
    xfbml      : true  // parse XFBML
  });

  FB.getLoginStatus(function(response) {
  if (response.status === 'connected') {
    // the user is logged in and has authenticated your
    // app, and response.authResponse supplies
    // the user's ID, a valid access token, a signed
    // request, and the time the access token 
    // and signed request each expire
    var uid = response.authResponse.userID;
    var accessToken = response.authResponse.accessToken;
        $('#facebook-post').show();
  } });
  };

  // Load the SDK asynchronously
  (function(d){
   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement('script'); js.id = id; js.async = true;
   js.src = "//connect.facebook.net/en_US/all.js";
   ref.parentNode.insertBefore(js, ref);
  }(document));
	</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="http://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="http://blueimp.github.io/JavaScript-Load-Image/js/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<!-- blueimp Gallery script -->
<script src="http://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="/js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
  </body>
</html>
