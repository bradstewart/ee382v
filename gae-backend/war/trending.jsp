<html lang="en">
<%@include file="header.jsp" %>
<link rel="stylesheet" type="text/css" href="css/als.css" />
<body>
<%@include file="navigation.jsp" %>

 <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">      
      <!-- START THE FEATURETTES -->

      <div class="row featurette">
        <div class="col-md-12">
         <h2 class="featurette-heading">Trending <span class="text-muted">Photo Streams.</span></h2>
          <p class="lead">Only the most popular images and streams.</p>
        <div class="als-container" id="trending-streams">
		  <span class="als-prev"></span>
		  <div class="als-viewport">	
			<div class="als-wrapper" id="display-trending-streams">
			    			    
			</div>
		</div>
	
  <span class="als-next"></span>
</div> <!-- als-container -->
</div> <!-- col-md-12 -->

</div> 
              

      <!-- /END THE FEATURETTES -->
<hr class="featurette-divider">
	
	
      <div class="row">
        <div class="well col-lg-6">
		<h3>Trending Report Rate</h3>
		<form action="/trendingrate" method="post">
		  <div class="radio">
			  <label>
			    <input type="radio" name="updateRate" id="optionsRadios-none" value="none" >
			    No Reports
			  </label>
		  </div>
		  <div class="radio">
			  <label>
			    <input type="radio" name="updateRate" id="optionsRadios-five-min" value="five-min" checked>
			    Every 5 minutes
			  </label>
		  </div>
		  <div class="radio">
			  <label>
			    <input type="radio" name="updateRate" id="optionsRadios-hour" value="hour" >
			    Every hour
			  </label>
		  </div>
		  <div class="radio">
			  <label>
			    <input type="radio" name="updateRate" id="optionsRadios-day" value="day" >
			    Every day
			  </label>
		  </div>
		  		  
		  <div class="form-group">
		    <div class="col-lg-10">
		      <button type="submit" class="btn btn-primary">Update Rate</button>
		    </div>
		  
		  </div>
		</form>
		
		</div>

	</div>
	
    <%@include file="footer.jsp" %>
    </div> <!-- Marketing Container -->
	<%@include file="javascript.jsp" %>
			<script type="text/javascript" src="js/jquery.als-1.2.min.js"></script>
	
    <script>
	//AJAX handler to display content for "viewall.html"
	$(document).ready( function() {	
		$('ul.nav li').removeClass("active");
		$('#nav-trending').addClass("active");
		
		
		
		var postTrending = $.post('/trending');
		postTrending.done( function( data ) {
			$("#display-trending-streams").html( data );
			$("#trending-streams").als({
				visible_items: 3,
				scrolling_items: 0,
				orientation: "horizontal",
				circular: "yes",
				autoscroll: "no",
				start_from: 1
			});
		});		
	});
	</script>
		    
  </body>
</html>
